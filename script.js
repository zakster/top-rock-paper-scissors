let playerScore = 0;
let computerScore = 0;

let playerSelection;
let computerSelection;

// store the value of 'win'/'lose' for the player
let isPlayerWin;

const buttons = document.getElementsByTagName('button');
const result = document.getElementById('result')

function getComputerChoice() {
	const objects = ['rock', 'paper', 'scissors'];
	return objects[Math.floor((Math.random() * 3))];
}

function playRound(playerSelection, computerSelection) {
	// winning conditions
	if ((playerSelection == 'paper' && computerSelection == 'rock') ||
		(playerSelection == 'scissors' && computerSelection == 'paper') ||
		(playerSelection == 'rock' && computerSelection == 'scissors')) {
		return isPlayerWin = 'win';
	} else if (playerSelection == computerSelection) {
		return isPlayerWin = 'draw';
	} else {
		return isPlayerWin = 'lose';
	}
}

function checkWinnerOfGame(playerScore, computerScore) {
	if (playerScore == 5) {
		result.textContent = 'Player Wins!';
		result.classList.add('win')
	} else if (computerScore == 5) {
		result.textContent = 'Computer Wins!';
		result.classList.add('lose')
	}
}

function checkWinnerOfRound(isPlayerWin) {
	switch (isPlayerWin) {
		case 'win':
			playerScore += 1;
			result.textContent = 'You Win! ' + playerSelection + ' beats ' + computerSelection;
			pscore.textContent = playerScore
			break;
		case 'lose':
			computerScore += 1;
			result.textContent = 'You Lose! ' + computerSelection + ' beats ' + playerSelection;
			cscore.textContent = computerScore
			break;
		case 'draw':
			result.textContent = 'Draw! ' + computerSelection + ' can\'t beat ' + playerSelection;
			break;
	}
}

function game() {
	for (let i = 0; i < buttons.length; i++) {
		buttons[i].addEventListener('click', () => {
			if (playerScore < 5 && computerScore < 5) {
				playerSelection = buttons[i].id;
				computerSelection = getComputerChoice();
				isPlayerWin = playRound(playerSelection, computerSelection);
				checkWinnerOfRound(isPlayerWin);
				checkWinnerOfGame(playerScore, computerScore);
			} 
		});
	}
}

game();
